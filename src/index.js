import React from 'react';
import ReactDOM from 'react-dom';

import {DashboardCustom} from "./DashboardCustom";
import './styles/main.scss';
import "animate.css";

ReactDOM.render(
    <DashboardCustom/>,
    document.getElementById('root')
);
