import React from 'react';

export const Footer = () => {
    return (

        <div className="footer__container">
            <div>
                <h1>GA</h1>
            </div>
            <div>
                <h1>GA</h1>
            </div>
            <div>
                <h5 className="text-center mt-3 mb-4">Contact us</h5>
                <div className="footer__contact-us">
                    <p>Direccion: <span>Av Los Angeles 123</span> </p>
                    <p>Correo: <span>correo@gmail.com</span> </p>
                    <p>Website: <span>www.website.com</span> </p>
                    <p>Celular: <span>987654321</span> </p>
                </div>
            </div>
        </div>

    )
};