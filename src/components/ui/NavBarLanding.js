import React from 'react';
import {Link, NavLink} from 'react-router-dom'

export const NavBarLanding = () => {
    return (

        <nav className="navbar navbar-expand-lg navbar-dark bg-primary px-4">
            <div className="container-fluid">

                <Link
                    className="navbar-brand"
                    to="/"
                >
                    Customize
                </Link>

                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText"
                        aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"> </span>
                </button>

                <div className="collapse navbar-collapse" id="navbarText">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">

                        <li className="nav-item">

                            <NavLink
                                activeClassName="active"
                                className="nav-link"
                                aria-current="page"
                                exact
                                to="/landing/dashboard"
                            >
                                Dashboards
                            </NavLink>

                        </li>

                        <li className="nav-item">
                            <NavLink
                                activeClassName="active"
                                className="nav-link"
                                exact
                                to="/landing/logins"
                            >
                                Logins
                            </NavLink>
                        </li>

                        <li className="nav-item">
                            <NavLink
                                activeClassName="active"
                                className="nav-link"
                                exact
                                to="/landing/contact"
                            >
                                Contact
                            </NavLink>
                        </li>

                    </ul>

                    <span className="navbar-text ">

                        <NavLink
                            // activeClassName="active"
                            className="nav-link navBar__button-login"
                            exact
                            to="/landing/login"
                        >
                            Login
                        </NavLink>
                    </span>

                </div>

            </div>

        </nav>

    )
};