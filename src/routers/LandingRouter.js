import React from 'react';
import { Redirect, Route, Switch } from "react-router-dom";

import { NavBarLanding } from "../components/ui/NavBarLanding";
import { Footer } from "../components/ui/Footer";

import { HomeScreen } from "../components/landing/HomeScreen";
import { DashboardScreen } from "../components/landing/DashboardScreen";
import { LoginsScreen } from "../components/landing/LoginsScreen";
import { ContactScreen } from "../components/landing/ContactScreen";
import { LoginScreen } from "../components/landing/auth/LoginScreen";

export const LandingRouter = () => {
    return (

        <div className="animate__animated animate__fadeIn">

            <NavBarLanding />

            <Switch>

                <Route exact path="/landing/home" component={HomeScreen} />
                <Route exact path="/landing/dashboard" component={DashboardScreen} />
                <Route exact path="/landing/login" component={LoginScreen} />
                <Route exact path="/landing/contact" component={ContactScreen} />
                <Route exact path="/landing/logins" component={LoginsScreen} />

                <Redirect to="/landing/home"/>

            </Switch>

            <Footer />
        </div>

    )
};