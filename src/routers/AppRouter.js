import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route, Redirect,
} from "react-router-dom";

import {DashboardRoute} from "./DashboardRoute";
import {LandingRouter} from "./LandingRouter";

export const AppRouter = () => {
    return (

        <Router>
            <div>

                <Switch>

                    <Route path="/dashboard" component={DashboardRoute}/>
                    <Route path="/landing"   component={LandingRouter}/>

                    <Redirect to="/landing"/>

                </Switch>

            </div>
        </Router>

    )
};